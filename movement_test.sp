#include <sourcemod>
#include <tf2>
#include <tf2_stocks>

#define CLIENT_MAX 32
#define JUMP_COOLDOWN_TIME 1.5 
#define JUMP_SOUND_SAMPLE_NAME "items/cart_explode.wav"
#define LOW_GRAVITY 200
#define LOW_GRAVITY_TIME 20.0
#define HIGH_GRAVITY 10000
#define HIGH_GRAVITY_TIME 1.0

float last_jump_times[ CLIENT_MAX ];
float gravity_time;
bool is_low_gravity;

public Plugin myinfo = {
	name = "movement test",
	author = "dan",
	description = "movement test",
	version = "1.0",
	url = "https://www.tf2nerds.com"
};

public void OnPluginStart() {
	PrintToServer( "movement test!" );
	PrecacheSound( JUMP_SOUND_SAMPLE_NAME, true );
	is_low_gravity = true;
	gravity_time = GetGameTime();
}

public OnGameFrame() {
	float current_time = GetGameTime();
	if ( is_low_gravity ) {
		if ( ( current_time - gravity_time ) > LOW_GRAVITY_TIME ) {
			SetConVarInt( FindConVar( "sv_gravity" ), HIGH_GRAVITY );
			is_low_gravity = false;
			gravity_time = current_time;
		}	
	} else {
		if ( ( current_time - gravity_time ) > HIGH_GRAVITY_TIME ) {
			SetConVarInt( FindConVar( "sv_gravity" ), LOW_GRAVITY );
			is_low_gravity = true;
			gravity_time = current_time;
		}
	}
}

public Action:OnPlayerRunCmd( int client, int& buttons, int& impulse,
			     	float vel[ 3 ], float angles[ 3 ],
				int& weapon, int& subtype,
				int& cmdnum, int& tickcount, int& seed,
				int mouse[ 2 ] ) {
	if ( buttons & IN_RELOAD ) {
		float current_time = GetGameTime();
		float last_jump_time = last_jump_times[ client ];
		if ( ( current_time - last_jump_time ) < JUMP_COOLDOWN_TIME ) {
			return Plugin_Continue;
		}

		last_jump_times[ client ] = current_time;

		int flags = GetEntityFlags( client );
		flags &= ~FL_ONGROUND;
		SetEntityFlags( client, flags );

		float eye_angles[ 3 ];
		GetClientEyeAngles( client, eye_angles );

		float eye_forward_vector[ 3 ];
		GetAngleVectors( eye_angles, eye_forward_vector, NULL_VECTOR, NULL_VECTOR );

		float abs_velocity[ 3 ];
		GetEntPropVector( client, Prop_Data, "m_vecAbsVelocity", abs_velocity );

		float teleport_velocity[ 3 ];
		teleport_velocity[ 0 ] = abs_velocity[ 0 ] + ( eye_forward_vector[ 0 ] * 700.0 );
		teleport_velocity[ 1 ] = abs_velocity[ 1 ] + ( eye_forward_vector[ 1 ] * 700.0 );
		teleport_velocity[ 2 ] = abs_velocity[ 2 ] + ( eye_forward_vector[ 2 ] * 700.0 );
		TeleportEntity( client, NULL_VECTOR, NULL_VECTOR, teleport_velocity );

		//EmitSoundToAll( JUMP_SOUND_SAMPLE_NAME );

		return Plugin_Changed;
	}

	return Plugin_Continue;
}

